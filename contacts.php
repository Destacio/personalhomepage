﻿<?php
	
	if(isset($_POST["submit"])) 
	{
		include("php/ContactForm.php");
	}
	
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>Контакты</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
	
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="js/jquery.validate.min.js" type="text/javascript"></script>
	<script src="js/myscripts.js" type="text/javascript"></script>  
	
</head>
<body>

	<div class="container">
	
	<ul class="nav nav-pills">
		<li role="presentation"><a href="index.php">Главная</a></li>
		<li role="presentation"><a href="portfolio.php">Портфолио</a></li>
		<li role="presentation" class="active"><a href="contacts.php">Контакты</a></li>
	</ul>

	<h1>Контакты</h1>
	
	<p>Фамилия Имя Отчество<br>
	Вконтакте: <a href="http://vk.com/">vk.com</a></p>
	
	<div id="contact-wrapper">
	
	<?php 

		$standartMessage = "Оставьте мне сообщение:";
		$messageSent = "Ваше сообщение отправлено, спасибо за отзыв!";
		
		if (isset($errors)) 
		{
			foreach($errors as $error) echo "<p class='error'>" . $error . "</p>";
		}
		elseif (isset($_POST['submit'])) echo $messageSent;
			else echo $standartMessage;
	
	?>
	
	<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="contactform">
		<div>
			<label for="contactname"><strong>Имя:</strong></label>
			<input type="text" size="50" name="contactname" id="contactname" value="<?=$name?>" />
		</div>
		<div>
			<label for="email"><strong>Email:</strong></label>
			<input type="text" size="50" name="email" id="email" value="<?=$email?>"/>
		</div>
		<div>
			<label for="message"><strong>Сообщение:</strong></label>
			<textarea rows="5" cols="59" name="message" id="message"><?=$message?></textarea>
		</div>
		<input type="submit" value="Отправить" name="submit" />
	</form>
	</div>
	
		<footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>
	
	</div>
	
	<!-- jQuery -->

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>