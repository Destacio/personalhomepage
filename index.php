<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">

    <title>Main page</title>

    <!-- Bootstrap Core CSS -->
	
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body>

    <!-- Navigation -->
    

    <!-- Full Page Image Background Carousel Header -->
    <header>
    </header>

    <!-- Page Content -->
    <div class="container">
	
	<ul class="nav nav-pills">
		<li role="presentation" class="active"><a href="index.php">Главная</a></li>
		<li role="presentation"><a href="portfolio.php">Портфолио</a></li>
		<li role="presentation"><a href="contacts.php">Контакты</a></li>
	</ul>

        <div class="row">
            <div class="col-lg-12">
                <h1>Welcome to my homepage!</h1>
				
            </div>
			<div class="col-lg-3">
				<img src="#" width="200" height="300">
			</div>
			<div class="col-lg-9">
				<p>Text text text text text text text text text text text
				 text text text text text text text text text text text text
				  text text text text text text text text text text text</p>
			</div>
        </div>

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
