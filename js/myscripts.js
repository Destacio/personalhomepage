﻿$(document).ready(function(){
	
	$("#contactform").validate({
		
		rules:{
			contactname:{
				
			},
			email:{
				required: true,
				email: true,
			},
			message:{
				required: true,
				maxlength: 500,
			}
		},
		
		messages: {
			contactname:{
				
			},
			email:{
				required: "Это поле обязательно для заполнения",
				email: "Введите корректный email",
			},
			message:{
				required: "Это поле обязательно для заполнения",
				maxlength: "Сообщение не должно превышать 500 символов",
			}
		}
	});
	
}); 