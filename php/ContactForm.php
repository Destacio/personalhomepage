﻿<?php

	$name = filter_var($_POST['contactname'], FILTER_SANITIZE_STRING);
	$email = filter_var($_POST['email'], FILTER_SANITIZE_STRING);
	$message = filter_var($_POST['message'], FILTER_SANITIZE_STRING);
	
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$errors[] = "Введенный email не является корректным!<br>";
	}
	if(strlen($message) > 500){
		$errors[] = "Длина сообщения превышает допустимую!<br>";
	}
	
	if(!isset($errors)){
		
		$text = array("$name", "$email", "$message");
		
		$fp = fopen('messages.csv', 'a');
		
		fputcsv($fp, $text, ';');
		
		fclose($fp);
		
		$name = $email = $message = "";
	}
	
	
?>