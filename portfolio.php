﻿<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">

    <title>Портфолио</title>

    <!-- Bootstrap Core CSS -->
	
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body>

    <!-- Navigation -->
    

    <!-- Full Page Image Background Carousel Header -->
    <header>
    </header>

    <!-- Page Content -->
    <div class="container">
	
	<ul class="nav nav-pills">
		<li role="presentation"><a href="index.php">Главная</a></li>
		<li role="presentation" class="active"><a href="portfolio.php">Портфолио</a></li>
		<li role="presentation"><a href="contacts.php">Контакты</a></li>
	</ul>

        <div class="row">
            <div class="col-lg-12">
                <h1>Мои проекты:</h1>	
            </div>
			<div class="col-lg-12">
				<a href="#"><img src="#" width="150" height="150"></a>
				<a href="#"><img src="#" width="150" height="150"></a>
				<a href="#"><img src="#" width="150" height="150"></a>
				<a href="#"><img src="#" width="150" height="150"></a>
				<a href="#"><img src="#" width="150" height="150"></a>
				<a href="#"><img src="#" width="150" height="150"></a>
				<a href="#"><img src="#" width="150" height="150"></a>
				<a href="#"><img src="#" width="150" height="150"></a>
			</div>
        </div>

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

</body>

</html>
